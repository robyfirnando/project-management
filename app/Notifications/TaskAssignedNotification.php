<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskAssignedNotification extends Notification
{
    use Queueable;
    private $task;

    public function __construct($task)
    {
        $this->task = $task;
    }

    public function via($notifiable)
    {
        return ['database']; // Only use the database channel
    }

    public function toArray($notifiable)
    {
        return [
            'message' => 'You have been assigned a new task: ' . $this->task->title,
            'task_id' => $this->task->id
        ];
    }
}
