<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function markAsRead(Request $request, $notificationId)
    {
        $user = auth()->user();
        $notification = $user->notifications()->find($notificationId);

        if ($notification) {
            $notification->markAsRead();
        }

        // Check if a redirect route is provided
        if ($request->has('redirect') && $request->has('task_id')) {
            // Redirect to the given route, in this case, task edit page
            return redirect()->route($request->redirect, ['task' => $request->task_id]);
        }

        return back()->with('error', 'Notification read, but no redirect found.');
    }
}
