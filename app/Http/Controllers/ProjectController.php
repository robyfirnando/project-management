<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Task;
use RealRashid\SweetAlert\Facades\Alert;

class ProjectController extends Controller
{
    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Failed !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('projects.index');
    }

    public function datatable(Request $request) {
        $model = Project::query();

        $dTable = DataTables()->of($model)
        ->addIndexColumn()
        ->editColumn('created_at', function ($project) {
            return $project->created_at ? $project->created_at->format('d/M/Y') : '';
        });

        return $dTable->toJson();
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            // 'progress' => 'required|numeric|min:0|max:100',
            // 'status' => 'required'
        ]);

        try {
            $p = new Project();
            $p->user_id = auth()->id();
            $p->title = $request->title;
            $p->description = $request->description;
            $p->progress = 0;
            $p->status = $request->has('status') ? 'Active' : 'Inactive';
            $p->save();

        } catch (\Exception $e) {
            return redirect()->route('backsite.project.index')->with('error_msg', 'Create Project failed.');
        }
        return redirect()->route('backsite.project.index')->with('success', 'Project created successfully.');
    }

    public function show(Project $project)
    {
        $taskCounts = Task::selectRaw('status, COUNT(*) as count')
                    ->where('project_id', $project->id)
                    ->groupBy('status')
                    ->pluck('count', 'status');

        return view('projects.show', ['taskCounts' => $taskCounts]);
    }

    public function edit(Project $project)
    {
        return view('projects.edit', ['project' => $project]);
    }

    public function update(Request $request, Project $project)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            // 'progress' => 'required|numeric|min:0|max:100',
            // 'status' => 'required'
        ]);

        try {
            $p = Project::find($project->id);
            $p->user_id = auth()->id();
            $p->title = $request->title;
            $p->description = $request->description;
            $p->progress = $project->calculateProgress();
            $p->status = $request->has('status') ? 'Active' : 'Inactive';
            $p->save();

        } catch (\Exception $e) {
            return redirect()->route('backsite.project.index')->with('error_msg', 'Update Project failed.');
        }
        return redirect()->route('backsite.project.index')->with('success', 'Project updated successfully.');
    }

    public function destroy(Project $project)
    {
        try {
            $project->delete();

        } catch (\Exception $e) {
            return redirect()->route('backsite.project.index')->with('error_msg', 'Delete Project failed.');
        }
        return redirect()->route('backsite.project.index')->with('success', 'Project deleted successfully.');
    }
}
