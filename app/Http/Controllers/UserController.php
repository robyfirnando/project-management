<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    //
    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Failed !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('users.index');
    }

    public function datatable() {
        $model = User::where('id', '!=', auth()->id());

        $dTable = DataTables()->of($model)
        ->addIndexColumn()
        ->editColumn('created_at', function ($user) {
            return $user->created_at ? $user->created_at->format('d/M/Y') : '';
        });

        return $dTable->toJson();
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
        ]);

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            $user->sendEmailVerificationNotification();

        } catch (\Exception $e) {
            return redirect()->route('backsite.user.index')->with('error_msg', 'Create User failed.');
        }

        return redirect()->route('backsite.user.index')->with('success', 'User created successfully.');
    }

    public function show(User $user)
    {
        return view('users.show', ['user' => $user]);
    }

    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'nullable|min:6',
        ]);

        try {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->filled('password') ? bcrypt($request->password) : $user->password,
            ]);
        } catch (\Exception $e) {
            return redirect()->route('backsite.user.index')->with('error_msg', 'Update User failed.');
        }

        return redirect()->route('backsite.user.index')->with('success', 'User updated successfully.');
    }

    public function destroy(User $user)
    {
        try {
            $user->delete();

        } catch (\Exception $e) {
            return redirect()->route('backsite.user.index')->with('error_msg', 'Delete User failed.');
        }
        return redirect()->route('backsite.user.index')->with('success', 'User deleted successfully.');
    }
}
