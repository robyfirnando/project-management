<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Notifications\TaskAssignedNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function datatable(Request $request) {
        $model = Task::where([
            'project_id' => $request->project_id,
            'status' => $request->status,
        ]);

        $dTable = DataTables()->of($model)
        ->addIndexColumn()
        ->editColumn('created_at', function ($task) {
            return $task->created_at ? $task->created_at->format('d/M/Y') : '';
        });

        return $dTable->toJson();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $projectId = request()->query('project_id');
        $p = Project::findOrFail($projectId);
        $data['project'] = $p;
        if (empty($projectId))
            abort(403, 'Parameter project_id is missing ');

        $data['users'] = User::all();

        return view('task.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'project_id' => 'required|exists:projects,id',
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'assign_to' => 'required|exists:users,id',
            'deadline' => 'required|date',
            'status' => 'required|in:todo,onprogress,done',
        ]);

        $dl = \Carbon\Carbon::createFromFormat('m/d/Y g:i A', $request->deadline)->format('Y-m-d H:i:s');

        $task = new Task();
        $task->project_id = $request->project_id;
        $task->title = $request->title;
        $task->description = $request->description;
        $task->assigned_to = $request->assign_to;
        $task->progress = 0;
        $task->deadline = $dl;
        $task->status = $request->status;

        $task->save();
        $user = User::find($request->assign_to);
        $user->notify(new TaskAssignedNotification($task));

        return redirect()->route('backsite.task.show', ['task' => $request->project_id, 'type' => $request->status])->with('success', 'Task created successfully!');
    }


    /**
     * Display the specified resource.
     */
    public function show(int $projectId)
    {
        $p = Project::with(['tasks'])->whereId($projectId)->firstOrFail();
        $data['project'] = $p;

        if (!empty(session('error_msg')))
            Alert::error('Failed !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('task.index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $task = Task::findOrFail($id);

        $user = auth()->user();
        $notification = $user->notifications()->find($id);
        if ($notification) {
            $notification->markAsRead();
        }
    
        
        $data['task'] = $task;
        $data['project'] = Project::find($task->project_id);
        $data['users'] = User::all();
        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $task->deadline);

        $formattedDateTime = $dateTime->format('m/d/Y h:i A');
        $data['newDl'] = $formattedDateTime;

        return view('task.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $task = Task::findOrFail($id);

        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'deadline' => 'required',
            'assign_to' => 'required|exists:users,id',
            'status' => 'required|in:todo,onprogress,done',
        ]);

        $task->title = $validatedData['title'];
        $task->description = $validatedData['description'];
        $task->deadline = Carbon::createFromFormat('m/d/Y h:i A', $validatedData['deadline'])->format('Y-m-d H:i:s');
        $task->assigned_to = $validatedData['assign_to'];
        $task->status = $validatedData['status'];

        $task->save();

        return redirect()->route('backsite.task.show', ['task' => $request->project_id, 'type' => $request->status])->with('success', 'Task updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $task = Task::find($id);
            $task->delete();

        } catch (\Exception $e) {
            return redirect()->route('backsite.task.show', ['task' => request()->project_id, 'type' => request()->status])->with('error_msg', 'Delete Task failed.');
        }
        return redirect()->route('backsite.task.show', ['task' => request()->project_id, 'type' => request()->status])->with('success', 'Task deleted successfully.');
    }
}
