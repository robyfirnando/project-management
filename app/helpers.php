<?php

function showNotif() {
    $user = auth()->user();

    $notifications = $user->unreadNotifications;

    return $notifications;
}