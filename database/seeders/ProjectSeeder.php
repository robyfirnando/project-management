<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $arr = [];

        $user = User::first();
        $p = new Project();
        $p->user_id = $user->id;
        $p->title = 'Project 1';
        $p->description = 'Lorem ipsum dolor sit amet';
        $p->progress = 0;
        $p->status = 'active';
        $p->save();
    }
}
