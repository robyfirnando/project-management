<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);
Route::get('/logout', [
    'uses' => 'App\Http\Controllers\Auth\LoginController@logout',
    'as' => 'logout',
]);
Route::get('/notifications/{notification}/read', 'App\Http\Controllers\NotificationController@markAsRead')->name('notifications.read');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['prefix' => 'backsite', 'namespace' => 'App\Http\Controllers', 'middleware' => 'auth', 'as' => 'backsite.' ], function () {
    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    // Route::get('/home', [
    //     'uses' => 'HomeController@index',
    //     'as' => 'backsite.beranda.index',
    // ]);

    Route::resource('dashboard', 'HomeController');

    Route::resource('project', 'ProjectController');
    Route::resource('user', 'UserController');
    Route::resource('task', 'TaskController');


    Route::group(['prefix' => 'datatable', 'as' => 'dtable.'], function () {
        Route::get('/project', [
            'uses' => 'ProjectController@datatable',
            'as' => 'project',
        ]);

        Route::get('/task', [
            'uses' => 'TaskController@datatable',
            'as' => 'task',
        ]);

        Route::get('/user', [
            'uses' => 'UserController@datatable',
            'as' => 'user',
        ]);
    });
});
