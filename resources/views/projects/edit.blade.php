@extends('layouts.app-master')
@section('menuProject', 'active')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="POST" action="{{ route('backsite.project.update', $project->id) }}" class="form-horizontal">
                        @method('PUT')
                        @csrf
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h4 class="card-title">Update Project {{ $project->title }}</h4>
                        </div>
                        <div class="card-content">
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Title</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" name="title" placeholder="Title" value="{{ $project->title }}">
                                        <!-- <span class="help-block">A block of help text that breaks onto a new line.</span> -->
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Description</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <textarea class="form-control" placeholder="Description" name="description">{{ $project->description }}</textarea>
                                        <!-- <span class="help-block">A block of help text that breaks onto a new line.</span> -->
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Status</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="status" {{ $project->status == 'active' ? 'checked' : '' }}> Active ?
                                            </label>
                                        </div>
                                        <!-- <span class="help-block">A block of help text that breaks onto a new line.</span> -->
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="row">
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection