@extends('layouts.app-master')

@section('menuProject', 'active')

@section('style')
<style>
.card-content {
    width: 40%;
    height: 40%;
}
</style>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h4 class="card-title">Project Stats (Task Status Distribution)</h4>
                    </div>
                    <div class="card-content">
                        <canvas id="statusChart" width="200" height="200"></canvas>
                        <div class="chart-notes">
                            <p><strong>Note:</strong> This chart displays the distribution of tasks across different statuses within the project. Each color represents a different status as follows:</p>
                            <ul>
                                <li><span style="color: rgba(255, 99, 132, 0.5);">●</span> Todo</li>
                                <li><span style="color: rgba(54, 162, 235, 0.5);">●</span> Onprogress</li>
                                <li><span style="color: rgba(255, 206, 86, 0.5);">●</span> Done</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('statusChart').getContext('2d');
    var statusChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: @json($taskCounts->keys()),
            datasets: [{
                label: 'Task Distribution',
                data: @json($taskCounts->values()),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(153, 102, 255, 0.5)',
                    'rgba(255, 159, 64, 0.5)'
                ],
                borderColor: [
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)'
                ],
                borderWidth: 2
            }]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                }
            }
        }
    });
});
</script>
@endsection
