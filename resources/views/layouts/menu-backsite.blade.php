<div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="https://cdn-icons-png.flaticon.com/512/3177/3177440.png" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                {{ auth()->user()->name }}
                <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li>
                        <a href="{{ route('logout') }}">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="nav">
        <li class="@yield('menuDashboard')">
            <a href="{{ route('backsite.dashboard.index') }}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="@yield('menuProject')">
            <a href="{{ route('backsite.project.index') }}">
                <i class="material-icons">apartment</i>
                <p>Projects</p>
            </a>
        </li>
        <li class="@yield('menuUser')">
            <a href="{{ route('backsite.user.index') }}">
                <i class="material-icons">group</i>
                <p>Users</p>
            </a>
        </li>
        <!-- <li>
            <a href="charts.html">
                <i class="material-icons">timeline</i>
                <p>Charts</p>
            </a>
        </li>
        <li>
            <a href="calendar.html">
                <i class="material-icons">date_range</i>
                <p>Calendar</p>
            </a>
        </li> -->
    </ul>
</div>