@extends('layouts.app-master')
@section('menuDashboard', 'active')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">language</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Welcome back {{ auth()->user()->name }}!</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Project Management Project</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection