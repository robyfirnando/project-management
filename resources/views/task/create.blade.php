@extends('layouts.app-master')

@section('menuProject', 'active')

@section('style')
<style>
    .radio label {
        cursor: pointer;
        padding-left: 35px;
        position: relative;
        color: black;
    }
    .help-block {
        color: red;
    }
</style>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="POST" action="{{ route('backsite.task.store') }}" class="form-horizontal">
                        @csrf
                        <input type="hidden" value="{{ request()->query('project_id') }}" name="project_id">

                        <div class="card-header card-header-text" data-background-color="rose">
                            <h4 class="card-title">Create Task For {{ $project->title }}</h4>
                        </div>
                        <div class="card-content">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Title</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating {{ $errors->has('title') ? 'has-error' : '' }}">
                                        <input type="text" class="form-control" name="title" placeholder="Title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <span class="help-block">{{ $errors->first('title') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Description</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating {{ $errors->has('description') ? 'has-error' : '' }}">
                                        <textarea class="form-control" placeholder="Description" name="description">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">{{ $errors->first('description') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Deadline</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating {{ $errors->has('deadline') ? 'has-error' : '' }}">
                                        <input type="text" class="form-control datetimepicker" name="deadline" value="{{ old('deadline', date('m/d/Y')) }}"/>
                                        @if ($errors->has('deadline'))
                                            <span class="help-block">{{ $errors->first('deadline') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Assign To</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating {{ $errors->has('assign_to') ? 'has-error' : '' }}">
                                        <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" name="assign_to">
                                            <option disabled selected>Choose user</option>
                                            @forelse ($users as $u)
                                            <option value="{{ $u->id }}" {{ old('assign_to') == $u->id ? 'selected' : '' }}>{{ $u->name }}</option>
                                            @empty
                                            <p>No users available</p>
                                            @endforelse
                                        </select>
                                        @if ($errors->has('assign_to'))
                                            <span class="help-block">{{ $errors->first('assign_to') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 label-on-left">Status</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-5 checkbox-radios">
                                        @foreach(['todo' => 'Todo', 'onprogress' => 'Onprogress', 'done' => 'Done'] as $value => $label)
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="status" value="{{ $value }}" {{ old('status') == $value ? 'checked' : '' }}> {{ $label }}
                                            </label>
                                        </div>
                                        @endforeach
                                        @if ($errors->has('status'))
                                            <span class="help-block">{{ $errors->first('status') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.datetimepicker').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove',
            inline: true
        }
    });
</script>
@endsection
