@extends('layouts.app-master')
@section('menuProject', 'active')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('backsite.task.create') }}?project_id={{ $project->id }}" class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">add</i>
                    </a>
                    <div class="card-content">
                        <h4 class="card-title">Tasks of {{ $project->title }}</h4>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="{{ request()->query('type') == 'todo' ? 'active' : '' }}">
                                <a href="{{ route('backsite.task.show', $project->id) }}?type=todo">To Do</a>
                            </li>
                            <li class="{{ request()->query('type') == 'onprogress' ? 'active' : '' }}">
                                <a href="{{ route('backsite.task.show', $project->id) }}?type=onprogress">Onprogress</a>
                            </li>
                            <li class="{{ request()->query('type') == 'done' ? 'active' : '' }}">
                                <a href="{{ route('backsite.task.show', $project->id) }}?type=done">Done</a>
                            </li>
                        </ul>
                        <div class="material-datatables">
                            <table id="dtable" class="dtable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Assigned To</th>
                                        <th>Deadline</th>
                                        <th class="disabled-sorting text-right" width="20%">Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Assigned To</th>
                                        <th>Deadline</th>
                                        <th class="disabled-sorting text-right">Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
@endsection

@section('script')
<script>
    getDatatable()

    function getDatatable() {
        /*Datatable*/
        $('.dtable').DataTable().destroy();
        $('.dtable').DataTable({
            serverSide: true,
            processing: true,
            lengthChange: false,
            ajax: {
                type: 'GET',
                url: "{{ route('backsite.dtable.task') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    project_id: "{{ $project->id }}",
                    status: "{{ request()->query('type') }}",
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'deadline',
                    name: 'deadline'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row) {
                        return ` 
                        <form action="${route('backsite.task.destroy', {
                            id: row.id,
                        })}" method="POST" id="form_${row.id}">
                            @csrf
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="project_id" value="{{ $project->id }}">
                            <input type="hidden" name="status" value="{{ request()->query('type') }}">
                            <a type="button" rel="tooltip" class="btn btn-sm btn-success btn-round" data-original-title="" title="" href="${route('backsite.task.edit', {
                                id: row.id
                            })}">
                                <i class="material-icons">edit</i>
                            </a>

                            <button type="submit" rel="tooltip" class="btn btn-sm btn-danger btn-round btn_delete" id="${row.id}" data-original-title="" title="">
                                <i class="material-icons">delete</i>
                            </button>
                        </form>
                        `
                    }
                },
            ]
        })
    }

    $(document).on('click', '.btn_delete', function(e) {
        e.preventDefault()

        Swal.fire({
            icon: 'warning',
            title: 'Are You Sure ?',
            text: "Are you sure to delete this item!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it !'
        }).then((result) => {
            if (result.isConfirmed) {
                let id = $(this).attr('id')
                $("#form_" + id).submit()
            }
        });
    })
</script>
@endsection